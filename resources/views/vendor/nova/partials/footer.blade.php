<p class="mt-8 text-center text-xs text-80">
    <a href="https://nova.laravel.com" class="text-primary dim no-underline">Trivia</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Compwhiz LLC - By Kennedy Mutisya.
    <span class="px-1">&middot;</span>
    v1.0.1
</p>
